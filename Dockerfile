# syntax=docker/dockerfile:1

FROM ruby:2.7.4

ENV WORKDIR "/sanction-search"
ENV GEM_HOME "/usr/local/bundle"
ENV PATH "$GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH"

WORKDIR ${WORKDIR}

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD ["bin/rails", "server", "-b", "0.0.0.0"]

COPY Gemfile ${WORKDIR}/Gemfile
COPY Gemfile.lock ${WORKDIR}/Gemfile.lock

RUN apt update -y && \
    apt-get install -y --no-install-recommends curl gnupg2 && \
    # curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    # echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
    apt update -y && \
    apt-get install -y --no-install-recommends postgresql-client && \
    # apt-get install -y --no-install-recommends yarn nodejs && \
    apt-get clean && \
    apt-get autoremove -y && \
    gem install bundler:2.2.12 && \
    bundle config set jobs \${nproc}  && \
    bundle install

COPY . ${WORKDIR}
