class SanctionEntity < ApplicationRecord
  EXACT_MATCH_DISTANCE = 1
  DEFAULT_MATCH_DISTANCE = 0.3

  validates :name, presence: true

  scope :find_by_similarity, ->(name, match_distance: DEFAULT_MATCH_DISTANCE) {
    where("similarity(name, ?) >= #{match_distance}", name).
      select('*', "similarity(name, '#{name}') AS similarity").
      order(similarity: :desc)
  }
  scope :individuals, -> { where(entity_type: 'individual') }
  scope :organizations, -> { where(entity_type: nil) }

  def as_json(*)
    element = {
      id: id,
      entity_number: entity_number,
      name: name,
      entity_type: entity_type,
      program: program,
      title: title,
      remarks: remarks,
    }
    return element.merge(similarity: similarity) if respond_to?(:similarity)

    element
  end
end
