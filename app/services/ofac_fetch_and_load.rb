module OfacFetchAndLoad
  require 'open-uri'
  require 'csv'

  OFAC_NULL_VALUE = '-0- '

  def self.call
    fetched_at = DateTime.now

    csv_uri = ENV.fetch('OFAC_SDN_CSV', 'https://www.treasury.gov/ofac/downloads/sdn.csv')

    SanctionEntity.transaction do
      CSV.foreach(
        URI.open(csv_uri),
        headers: %w[
          entity_number name type program program title _vessel_type _tonnage _grt
          _vessel_flag _vessel_owner remarks
        ]
      ) do |row|
        # The CSV file ends with a unsavory line. It has no content other than
        # `''`.
        # Given that entries without `name` field break, we're ensuring that
        # entries without this field get skipped
        next if sanitize(row['name']).nil?

        SanctionEntity.create(
          entity_number: sanitize(row['entity_number']),
          name: sanitize(row['name']),
          entity_type: sanitize(row['type']),
          program: sanitize(row['program']),
          title: sanitize(row['title']),
          remarks: sanitize(row['remarks']),
          fetched_at: fetched_at,
        )
      end
    end

    SanctionEntity.where('fetched_at != ?', fetched_at).delete_all
  end

  def self.sanitize(input)
    return nil if input == OFAC_NULL_VALUE

    input
  end
  private_class_method :sanitize

end
