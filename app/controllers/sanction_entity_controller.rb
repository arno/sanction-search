class SanctionEntityController < ApplicationController
  def search
    entries = SanctionEntity.find_by_similarity(name, match_distance: match_distance)

    return head(:not_found) if entries.empty?

    render(json: entries.to_json)
  end

  def individual_search
    individuals =
      SanctionEntity.
      individuals.
      find_by_similarity(name, match_distance: match_distance)

    return head(:not_found) if individuals.empty?

    render(json: individuals.to_json)
  end

  def organization_search
    organizations =
      SanctionEntity.
      organizations.
      find_by_similarity(name, match_distance: match_distance)

    return head(:not_found) if organizations.empty?

    render(json: organizations.to_json)
  end

  private

  def name
    params[:name] || ''
  end

  def match_distance
    return SanctionEntity::EXACT_MATCH_DISTANCE if params[:exact] == 'true'

    SanctionEntity::DEFAULT_MATCH_DISTANCE
  end
end
