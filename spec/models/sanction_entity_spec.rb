require 'rails_helper'

describe SanctionEntity do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
  end

  describe 'scopes' do
    describe '.find_by_similarity' do
      it 'accepts an argument for the name' do
        expect { described_class.find_by_similarity('Arno Fleming') }.to_not raise_error
      end

      it 'includes sanction entities that closely match the name' do
        exact_match = described_class.create(name: 'Arno Fleming')
        close_match = described_class.create(name: 'Close match Fleming')
        result = described_class.find_by_similarity('Arno Fleming')
        expect(result).to include(exact_match)
        expect(result).to include(close_match)
      end

      it 'does not include sanction entities that have no close match on the name' do
        no_close_match = described_class.create(name: 'No Match At All')
        result = described_class.find_by_similarity('Arno Fleming')
        expect(result).to_not include(no_close_match)
      end
    end

    describe '.individuals' do
      it 'returns sanction entities with entity_type "individual"' do
        individual = described_class.create(name: 'Arno Fleming', entity_type: 'individual')

        expect(described_class.individuals).to include(individual)
      end

      it 'does not return sanction entities with another entity_type' do
        _organization = described_class.create(name: 'Arno Fleming', entity_type: nil)
        _vessel = described_class.create(name: 'Arno Fleming', entity_type: 'vessel')
        _aircraft = described_class.create(name: 'Arno Fleming', entity_type: 'aircraft')

        expect(described_class.individuals).to be_empty
      end
    end

    describe '.organizations' do
      it 'returns sanction entities with entity_type that is `nil`' do
        organization = described_class.create(name: 'Arno Fleming', entity_type: nil)

        expect(described_class.organizations).to include(organization)
      end

      it 'does not return sanction entities with another entity_type' do
        _individual = described_class.create(name: 'Arno Fleming', entity_type: 'individual')
        _vessel = described_class.create(name: 'Arno Fleming', entity_type: 'vessel')
        _aircraft = described_class.create(name: 'Arno Fleming', entity_type: 'aircraft')

        expect(described_class.organizations).to be_empty
      end
    end
  end

  describe '#to_json' do
    it 'has the keys and values for [entity_number, name, entity_type, program, title, remarks]' do
      subject = described_class.new(
        entity_number: 'entity_number',
        name: 'name',
        entity_type: 'entity_type',
        program: 'program',
        title: 'title',
        remarks: 'remarks',
      )

      expect(
        JSON.parse(subject.to_json)
      ).to eq(
        'id' => subject.id,
        'entity_number' => 'entity_number',
        'name' => 'name',
        'entity_type' => 'entity_type',
        'program' => 'program',
        'title' => 'title',
        'remarks' => 'remarks'
      )
    end
  end
end
