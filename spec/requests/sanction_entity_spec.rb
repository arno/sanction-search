require 'rails_helper'

describe 'sanction_entity', type: :request do
  describe 'GET /search' do
    context 'with a positive match' do
      it 'returns http success' do
        SanctionEntity.create(name: 'Cow Bell')
        get '/sanction_entity/search', params: { name: 'Cow Bell' }
        expect(response).to have_http_status(:success)

        get '/sanction_entity/search', params: { name: 'Cattle Bell' }
        expect(response).to have_http_status(:success)
      end

      context 'with one matching entity' do
        it 'returns the entity as a collection' do
          cow_bell = SanctionEntity.create(name: 'Cow Bell')
          get '/sanction_entity/search', params: { name: 'Cow Bell' }

          expect(JSON.parse(response.body)).to eq [
            {
              'id' => cow_bell.id,
              'entity_number' => nil,
              'name' => 'Cow Bell',
              'entity_type' => nil,
              'program' => nil,
              'title' => nil,
              'remarks' => nil,
              'similarity' => 1.0
            }
          ]
        end

        context 'with multiple matching entities' do
          it 'returns all entities' do
            cow_bell = SanctionEntity.create(name: 'Cow Bell')
            cattle_bell = SanctionEntity.create(name: 'Cattle Bell')
            get '/sanction_entity/search', params: { name: 'Cow Bell' }

            collection = JSON.parse(response.body)
            expect(collection).to include(
              hash_including('id' => cow_bell.id)
            )

            expect(collection).to include(
              hash_including('id' => cattle_bell.id)
            )
          end
        end
      end
    end

    context 'without a positive match' do
      it 'returns 404 NOT FOUND' do
        SanctionEntity.create(name: 'Cow Bell')

        get '/sanction_entity/search', params: { name: 'Cymbal' }
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'GET /search/individuals' do
    it 'renders the collection from SanctionEntity.individuals' do
      expect(SanctionEntity).to receive(:individuals).and_call_original.once
      expect(SanctionEntity).
        to receive(:find_by_similarity).
        and_return([SanctionEntity.new(name: 'Arno Fleming')]).
        once

      get '/sanction_entity/search/individuals', params: { name: 'words' }
    end
  end

  describe 'GET /search/organizations' do
    it 'renders the collection from SanctionEntity.organizations' do
      expect(SanctionEntity).to receive(:organizations).and_call_original.once
      expect(SanctionEntity).
        to receive(:find_by_similarity).
        and_return([SanctionEntity.new(name: 'Arno Fleming')]).
        once

      get '/sanction_entity/search/organizations', params: { name: 'words' }
    end
  end
end
