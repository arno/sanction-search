Rails.application.routes.draw do
  get 'sanction_entity/search'
  get 'sanction_entity/search/individuals', to: 'sanction_entity#individual_search'
  get 'sanction_entity/search/organizations', to: 'sanction_entity#organization_search'
end
