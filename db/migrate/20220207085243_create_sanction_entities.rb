class CreateSanctionEntities < ActiveRecord::Migration[7.0]
  def change
    create_table :sanction_entities do |t|
      t.string :entity_number
      t.string :name
      t.string :entity_type
      t.string :program
      t.string :title
      t.string :remarks
      t.datetime :fetched_at

      t.timestamps
    end
  end
end
