class AddTrgmIndexToSanctionEntity < ActiveRecord::Migration[7.0]
  def up
    add_index(:sanction_entities, :name, using: :gin, opclass: :gin_trgm_ops, name: 'sanction_entities_name_trigram')
  rescue PG::UndefinedObject, StandardError => e
    puts
    puts '*** ERROR ***'
    puts 'This migration assumes the super user of the postgres install ran the command'
    puts '`CREATE EXTENSION pg_trgm`'
    puts
    puts 'Please enable that extension prior to re-running this migration'
    puts '*** ERROR ***'
    puts
    raise e
  end

  def down
    remove_index :sanction_entities, name: 'sanction_entities_name_trigram'
  end
end
