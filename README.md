# Sanction Search

## Loading / replacing OFAC entries

This can be done running:

```
rake ofac_entries:fetch_and_load OFAC_SDN_CSV=lib/files/sdn.csv
```

The OFAC_SDN_CSV variable can be local file (relative to your project directory), or a URL.
When not supplying the `OFAC_SDN_CSV` env variable, a fresh list will be pulled from https://www.treasury.gov/ofac/downloads/sdn.csv

Note: Since the app is dockerized (see [below](#your-development-environment)), ensure to not run this on your local machine only, but against the container.

## Search

The search endpoints lives on

- `GET /sanction_entity/search` (matching on all entity types)
- `GET /sanction_entity/search/individuals` (matching on individual persons only)
- `GET /sanction_entity/search/organizations` (matching on entities without entity type only)

All endpoints have `name` as optional argument. Note that the search will not be very useful when omitted.
When supplying the key and value 'exact=true' as params to these requests, only exact matches will be returned. Without the 'exact=true' combo, fuzzy searching is performed.

These endpoints will always return a collection of 1 or more SanctionEntities.
These collections consist of best matching entities, based on their name (unless exact matching is performed, see above).
The fuzzy search collection is ordered with the best match as first entry.
If exact matching is performed, the collection can still contain multiple entities

The fuzzy search is using a Postgres Trigram search.

### Example:
```
# these examples use httpie
> http "localhost:3000/sanction_entity/search?name=Ramon"
HTTP/1.1 200 OK
[
  {
    "entity_number": "10933",
    "entity_type": null,
    "name": "RAMAK",
    "program": "SYRIA",
    "remarks": "Website www.ramakdutyfree.net; Email Address dam.d.free@net.sy.",
    "similarity": 0.33333334,
    "title": null
  }
]

> http "localhost:3000/sanction_entity/search/individuals?name=RAMON MAGANA"
HTTP/1.1 200 OK
[
  {
    "entity_number": "6866",
    "entity_type": "individual",
    "name": "RAMON MAGANA, Alcides",
    "program": "SDNTK",
    "remarks": "DOB 04 Sep 1957.",
    "similarity": 0.61904764,
    "title": null
  },
  {
    "entity_number": "15688",
    "entity_type": "individual",
    "name": "REYES MAGANA, Felipe",
    "program": "SDNTK",
    "remarks": "DOB 11 Oct 1967; POB Tonila, Jalsico; C.U.R.P. REMF671011HJCYGL02 (Mexico); RFC REMF671011QH1 (Mexico); Linked To: OPERADORA Y ADMINISTRADORA DE RESTAURANTES Y BARES RUDU, S.A. DE C.V.; Linked To: CASA EL VIEJO LUIS DISTRIBUIDORA, S.A. DE C.V.",
    "similarity": 0.32,
    "title": null
  }
]

> http "localhost:3000/sanction_entity/search?name=RAMON MAGANA&exact=true"
HTTP/1.1 404 Not Found

> http "localhost:3000/sanction_entity/search?name=REYES MAGANA, Felipe&exact=true"
HTTP/1.1 200 OK

[
  {
    "entity_number": "15688",
    "entity_type": "individual",
    "id": 3136,
    "name": "REYES MAGANA, Felipe",
    "program": "SDNTK",
    "remarks": "DOB 11 Oct 1967; POB Tonila, Jalsico; C.U.R.P. REMF671011HJCYGL02 (Mexico); RFC REMF671011QH1 (Mexico); Linked To: OPERADORA Y ADMINISTRADORA DE RESTAURANTES Y BARES RUDU, S.A. DE C.V.; Linked To: CASA EL VIEJO LUIS DISTRIBUIDORA, S.A. DE C.V.",
    "similarity": 1.0,
    "title": null
  }
]
```

# Your development environment

This app is dockerized. This means you can run this app without adding additional software to your laptop _directly_, apart from, clearly, [docker](https://docs.docker.com/get-docker/) and docker-compose. If you want some GUI help on your MacOS/Windows, also install docker desktop.

After installing docker, starting the rails server and tailing the logs is as simple as

```terminal
docker-compose up -d
docker-compose logs --follow
```

Note that the above will bring up a postgres database alongside with the rails server.

In case you want to run guard, while developing software (and, be honest, who doesn't?), you can start that on its own, as it is not started by default.

```terminal
docker-compose run guard
```

This will pick up all your changes, and run guard whenever a spec or model changes.

## Running `rake`, `rails` or other commands

Running your migrations and other commands have to go a bit different though. Where on a non-dockerized app you run it locally, you now have to run it on a containers.
Do it by invoking something like

```terminal
# Running (a) migration(s)
docker compose run web rails db:migrate

# Starting a rails console or database terminal
docker compose run web rails console
docker compose run web rails db

# Installing the ofac entries
docker-compose run web rake ofac_entries:fetch_and_load OFAC_SDN_CSV=lib/files/sdn.csv

# Or try doing things with your postgresql container
docker-compose run db ls -la
```
