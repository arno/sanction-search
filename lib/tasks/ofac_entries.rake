namespace :ofac_entries do
  desc "fetch ofac entries from their website, and replace all ofac entries in our database"
  task fetch_and_load: :environment do
    OfacFetchAndLoad.call
  end
end
